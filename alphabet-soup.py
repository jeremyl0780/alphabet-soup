def processfile(filepath):
    with open(filepath) as fp:
        line = fp.readline()
        cnt = 1
        puzzle = ""
        solutions = []
        while line:
            if cnt == 1:
                rowCnt = int(line.strip()[:1])
                colCnt = int(line.strip()[2])
            else:
                if rowCnt > 0:
                    puzzle += line.strip() + "\n"
                    rowCnt -= 1
                else:
                    solutions.append(line.strip())
            line = fp.readline()
            cnt += 1

    wordgrid = puzzle.replace(' ','')

    length = wordgrid.index('\n')+1

    characters = [(letter, divmod(index, length))
                for  index, letter in enumerate (wordgrid)]

    wordlines = {}

    directions = {'going downwards':0, 'going downwards and left diagonally':-1, 'going downwards and right diagonally':1}

    for word_direction, directions in directions.items():
        wordlines[word_direction] = []
        for x in range(length):
            for i in range(x, len(characters), length + directions):
                wordlines[word_direction].append(characters[i])
            wordlines[word_direction].append('\n')

    wordlines['going right'] = characters
    wordlines['going left'] = [i for i in reversed(characters)]
    wordlines['going upwards'] = [i for i in reversed(wordlines['going downwards'])]
    wordlines['going upwards and left diagonally'] = [i for i in reversed(wordlines['going downwards and right diagonally'])]
    wordlines['going upwards and right diagonally'] = [i for i in reversed(wordlines['going downwards and left diagonally'])]

    printitout(word_direction, tuple, wordlines, solutions, filepath)

def printitout(direction, tuple, lines, solutions, filename):
    print("Results for ", filename, ":")
    for direction, tuple in lines.items():
        string = ''.join([i[0] for i in tuple])
        for word in solutions:
            if word in string:
                coordinates = tuple[string.index(word)][1]
                endingCoordinates1 = coordinates[0]
                endingCoordinates2 = coordinates[1]
                if "right" in direction:
                    endingCoordinates2 += len(word) - 1
                if "left" in direction:
                    endingCoordinates2 -= len(word) - 1
                if "up" in direction:
                    endingCoordinates1 -= len(word) - 1
                if "down" in direction:
                    endingCoordinates1 += len(word) - 1
                print(word, str(coordinates[0]) + ':' + str(coordinates[1]), str(endingCoordinates1) + ':' + str(endingCoordinates2))
    print("\n")

processfile("input1.txt")
processfile("input2.txt")